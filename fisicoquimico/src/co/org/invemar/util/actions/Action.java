/**
 * 
 */
package co.org.invemar.util.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Administrador
 *
 */
public interface Action {
	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response)
	throws java.io.IOException, javax.servlet.ServletException;
}

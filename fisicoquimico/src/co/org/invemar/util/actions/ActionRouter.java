/**
 * 
 */
package co.org.invemar.util.actions;

import javax.servlet.GenericServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Administrador
 * 
 */
public class ActionRouter {

	private final String url;

	private final boolean isForward;

	public ActionRouter(String url) {
		this(url, true); // forward by default
	}

	public ActionRouter(String url, boolean isForward) {
		this.url = url;
		this.isForward = isForward;
	}

	// This method is called by the action servlet
	public void route(GenericServlet servlet, HttpServletRequest request,
			HttpServletResponse response) throws javax.servlet.ServletException,
			java.io.IOException {
		if (isForward) {
			servlet.getServletContext()
					.getRequestDispatcher(response.encodeURL(url)).forward(request, response);
		} else {
			response.sendRedirect(response.encodeRedirectURL(url));
		}
	}
}

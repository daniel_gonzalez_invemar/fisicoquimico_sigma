package co.org.invemar.util;

import java.io.*;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
 
public class Cmail  {
/**
 * Clase que envia un email
 * autor: Rafael E. Lastra C.<br>
 * Version: 1.0 <br>
 * Fecha: 22-03-2002
*/
  private String a_host = "192.168.3.1";
  private String a_from;
  private String a_to;
  private String a_subject;
  private String a_mensaje;
  private String Error;

  /**
  Metodo constructor de la clase
  */
  public void cmail(){ }
  /**
  Carga el atributo a_host al objeto
  */
  public void setahost(String host){
    a_host = host;
  }
  /**
  Carga el atributo a_from al objeto
  */
  public void setafrom(String from){
    a_from = from;
  }
  /**
  Carga el atributo a_to al objeto
  */
  public void setato(String to){
    a_to = to;
  }
  /**
  Carga el atributo a_subject al objeto
  */
  public void setasubject(String subject){
    a_subject = subject;
  }
  /**
  Carga el atributo a_mensaje al objeto
  */
  public void setamensaje(String mensaje){
    a_mensaje = mensaje;
  }
  /**
  Retorna el atributo Error del servidor de correo
  */
  public String get_error(){
    return Error;
  }
  
  public boolean manda(){
	  
    boolean resp = true;
    // agarra las propiedades del sistema
    Properties props = System.getProperties();
    props.put("mail.smtp.host", a_host);
    Session session = Session.getDefaultInstance(props, null);
    try{
      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress(a_from));
      message.addRecipient(Message.RecipientType.TO,new InternetAddress(a_to));
      message.setSubject(a_subject);
      message.setContent(a_mensaje, "text/html");
      Transport.send(message);
    } catch(MessagingException mex) {
        Error = mex.getMessage();
        resp = false;
      }
    
    return resp;
  }

}

package co.org.invemar.model;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.block.EmptyBlock;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.Year;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DeviationChart {
	private SeriesDataManager series = null;
	private YIntervalSeries yintervalseries = null;
	private String data;
	private ArrayList listaDatos;
	private String measureUnit="unidad";
 
	
	
	private XYDataset getDatasetByAnyo(int nivel, String nombre, String sitio, String unidadManejo, String sector,
			String codigoEstacion,String variable, String cualidad) {
		series = new SeriesDataManager();
		yintervalseries = new YIntervalSeries(nombre);	
		yintervalseries = series.getYIntervalSeriesByAnyo(nivel,nombre, sitio, unidadManejo, sector, codigoEstacion, variable,cualidad);    
		
		
		data = series.getData();
		listaDatos= series.getListaDatos();
	    measureUnit =series.getMeasureUnit();
		 
		YIntervalSeriesCollection yintervalseriescollection = new YIntervalSeriesCollection();
		yintervalseriescollection.addSeries(yintervalseries);

		return yintervalseriescollection;
	}
	

	
	
	private JFreeChart createChart(String titulo, String timeAxisLabel,
			String valueAxisLabe,XYDataset xydataset){
		JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(titulo, timeAxisLabel,
				valueAxisLabe, xydataset, true, true, false);
		
		
		
		
		 XYPlot xyplot = (XYPlot)jfreechart.getPlot();
		 NumberAxis axis = new NumberAxis();
		
		 
		 DateAxis domainAxis = new DateAxis();
	     domainAxis.setDateFormatOverride(new SimpleDateFormat("yyyy"));	    
	    // domainAxis.setTickUnit(new DateTickUnit(DateTickUnitType.YEAR,2));
	    
		 
		 
		 xyplot.setDomainAxis(domainAxis);
		 
	     xyplot.setDomainPannable(true);
	     xyplot.setRangePannable(false);    
	     xyplot.setInsets(new RectangleInsets(5D, 5D, 5D, 20D));
	     
	     
	     
	     DeviationRenderer deviationrenderer = new DeviationRenderer(true, false);
	     deviationrenderer.setSeriesStroke(0, new BasicStroke(2F, 1, 1));
	     deviationrenderer.setSeriesFillPaint(0, new Color(255, 200, 200));
	     xyplot.setRenderer(0,deviationrenderer);	
      
        
	     
	  	return jfreechart;
	}
	
	
	public JFreeChart createChartFisicoquimicoByAnyo(int nivel, String titulo, String timeAxisLabel,
			String valueAxisLabe, String nombre, String sitio, String unidadManejo, String sector,
			String codigoEstacion,String variable, String cualidad) {
		XYDataset xydataset = null;
		JFreeChart jfreechart = null;
		
		xydataset = getDatasetByAnyo(nivel,nombre, sitio, unidadManejo, sector, codigoEstacion, variable, cualidad);
		
		jfreechart = createChart(titulo,timeAxisLabel,measureUnit ,xydataset);
	     
	  	return jfreechart;
	}




	public String getData() {
		return data;
	}




	public void setData(String data) {
		this.data = data;
	}




	public ArrayList getListaDatos() {
		return listaDatos;
	}




	public void setListaDatos(ArrayList listaDatos) {
		this.listaDatos = listaDatos;
	}




	private String getMeasureUnit() {
		return measureUnit;
	}




	private void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}
	
	
	

}

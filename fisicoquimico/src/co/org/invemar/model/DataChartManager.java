package co.org.invemar.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

public class DataChartManager {

	public String findSitios(int nivel, int codUnidadManejo)
			throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "SELECT DISTINCT sitio,nombrecompletositio,codsitio FROM manglares_nacionales_fq where codunidadmanejo=? and nivel=?  order by codsitio";
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setInt(1, codUnidadManejo);
			pstmt.setInt(2, nivel);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("codsitio"));
				objsData.put("name",
						rs.getString("codsitio") + "-" + rs.getString("sitio")
								+ "-" + rs.getString("nombrecompletositio"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findAdministrativeDepartament() throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = "SELECT TOPONIMO_TP,TOPONIMO_TEXTO "
				+ "                FROM CLST_TOPONIMIAT"
				+ "                WHERE categoria_tp    ='D' "
				+ "                and  (SUBSTR(region,1,2)='20' or SUBSTR(region,1,2)='13' or  SUBSTR(region,1,2)='10' ) "
				+ "                ORDER BY TOPONIMO_TEXTO";

		
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			conn = cFactory.createConnection("geograficos");
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();			

			while (rs.next()) {
				JSONObject objsData = new JSONObject();				
				objsData.put("id", rs.getString("TOPONIMO_TP"));
				objsData.put("name",rs.getString("TOPONIMO_TEXTO"));              
				arrayData.put(objsData);

			}

		} catch (SQLException e) {
			System.out.println("Error:"+e.getMessage());
  
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findUnidadManejo(int nivel, int sector) throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "";

		SQL_ = "SELECT DISTINCT unidad_manejo,codunidadmanejo FROM manglares_nacionales_fq WHERE nivel=? and codsector = ?";

		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setInt(1, nivel);
			pstmt.setInt(2, sector);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("codunidadmanejo"));
				objsData.put(
						"name",
						rs.getString("codunidadmanejo") + "-"
								+ rs.getString("unidad_manejo"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findSector(int nivel,String codDepartament) throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT codsector,sector,coddepartamento,nombrecompletosector "
                + "FROM "
                + "  (SELECT DISTINCT codsector, "
                + "    sector,nombrecompletosector, "
                + "    (SELECT valor2 "
                + "    FROM manglar.datos_casos_estudio "
                + "    WHERE tipo         =86 "
                + "    AND codigo_atributo=27 "
                + "    AND id             =codsector "
                + "    ) AS coddepartamento "
                + "  FROM manglares_nacionales_fq "
                + "  WHERE  nivel=? "
                + "  ) "
                + "WHERE coddepartamento=?";		
		
		
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {

			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, nivel);
			pstmt.setString(2, codDepartament);
			rs = pstmt.executeQuery();	

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("codsector"));
				objsData.put("name",rs.getString("codsector") + "-"+ rs.getString("sector") + "-"+ rs.getString("nombrecompletosector"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {
			Logger.getLogger(DataChartManager.class.getName()).log(
					Level.SEVERE, e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findSector(int nivel, String sitio, String unidad)
			throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "SELECT DISTINCT sector FROM manglares_nacionales_fq WHERE sitio = ? and unidad_manejo= ? and nivel=?";
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setString(1, sitio);
			pstmt.setString(2, unidad);
			pstmt.setInt(3, nivel);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("sector"));
				objsData.put("name", rs.getString("sector"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findEstacion(int codSitio) throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "SELECT DISTINCT codest, nombreparcela FROM manglares_nacionales_fq WHERE codsitio=? and codest is not null order by codest ";
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setInt(1, codSitio);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("codest"));
				objsData.put(
						"name",
						rs.getString("codest") + "-"
								+ rs.getString("nombreparcela"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findVariables(String sitio, String unidad, String sector,
			String estacion) throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "SELECT DISTINCT variable,utilidades.getNombreVariable(variable) as nombreVariable FROM manglares_nacionales_fq WHERE codsitio =? AND codunidadmanejo=?  AND codsector=? AND codest=?";
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setString(1, sitio);
			pstmt.setString(2, unidad);
			pstmt.setString(3, sector);
			pstmt.setString(4, estacion);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("variable"));
				objsData.put("name", rs.getString("nombreVariable"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findVariables(String estacion) throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL_ = "SELECT DISTINCT variable,utilidades.getNombreVariable(variable) as nombreVariable FROM manglares_nacionales_fq WHERE nivel=2 and codsitio=?";
		JSONObject objsData = null;
		JSONArray arrayData = new JSONArray();
		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			pstmt.setString(1, estacion);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("variable"));
				objsData.put("name", rs.getString("nombreVariable"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}

	public String findCualidades(int nivel, String sitio, String unidad,
			String sector, String estacion, String variable)
			throws JSONException {

		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		JSONArray arrayData = new JSONArray();
		JSONObject objsData = null;
		ResultSet rs = null;
		String SQL_ = "";

		if (nivel == 1) {
			SQL_ = "SELECT distinct cualidad FROM manglares_nacionales_fq WHERE nivel=1 and codsitio =? AND codunidadmanejo=? AND codsector=? AND codest=?  and variable=? ";
		} else if (nivel == 2) {
			SQL_ = "SELECT distinct cualidad FROM manglares_nacionales_fq WHERE nivel=2 and  codsitio =? and variable=?  ";
		}

		try {
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement(SQL_);
			if (nivel == 1) {
				pstmt.setString(1, sitio);
				pstmt.setString(2, unidad);
				pstmt.setString(3, sector);
				pstmt.setString(4, estacion);
				pstmt.setString(5, variable);

			} else if (nivel == 2) {
				pstmt.setString(1, sitio);
				pstmt.setString(2, variable);

			}

			rs = pstmt.executeQuery();

			while (rs.next()) {
				objsData = new JSONObject();
				objsData.put("id", rs.getString("cualidad"));
				objsData.put("name", rs.getString("cualidad"));
				arrayData.put(objsData);

			}

		} catch (SQLException e) {

			Logger.getLogger(DataChartManager.class.getName()).log(
					Level.SEVERE, e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}

		return arrayData.toString();

	}
	/**
	 * *************************************************************************
	 * ***********
	 */

}

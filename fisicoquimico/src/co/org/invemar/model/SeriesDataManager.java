package co.org.invemar.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.jfree.data.time.Year;

import org.jfree.data.xy.YIntervalSeries;
import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

public class SeriesDataManager {

	private YIntervalSeries yintervalseries = null;
	private JSONArray arrayData = null;
	private String data;
	private String measureUnit="unidad";

	private ArrayList listaDatos;

	public YIntervalSeries getYIntervalSeriesByAnyo(int nivel, String nombre,
			String sitio, String unidadManejo, String sector,
			String codigoEstacion, String variable, String cualidad) {

		/*
		 * SELECT ANO,PROM,desvest FROM manglares_nacionales_fq WHERE sitio =
		 * 'LUN' AND unidad_manejo= 'ZP1' AND sector='CGSM' AND codest='39466'
		 * AND cualidad='Sup' ORDER BY ano;
		 */

		yintervalseries = null;
		yintervalseries = new YIntervalSeries(nombre);
		ConnectionFactory cFactory = new ConnectionFactory();
		Connection conn = null;
		PreparedStatement pstmt = null;
		JSONObject objsData = null;
		ResultSet rs = null;
		String SQL_ = "";       
		if (nivel != 0) {
			if (nivel == 1) {
				SQL_ = "SELECT ANO,PROM,desvest, max, mix,unidad"
						+ " FROM manglares_nacionales_fq "
						+ " WHERE nivel=1 and codsitio =?  AND codunidadmanejo=?  AND codsector=? AND codest=?  AND variable=? AND cualidad=?  ORDER BY ano ";
			} else if (nivel == 2) {
				SQL_ = "SELECT ANO,PROM,desvest, max, mix,unidad"
						+ " FROM manglares_nacionales_fq "
						+ " WHERE nivel=2 and codsitio =?     AND variable=? AND cualidad=?  ORDER BY ano ";
			}
			/*System.out.println("Nivel:" + nivel);
			System.out.println("SQL_:" + SQL_);
			System.out.println("cualidad:" + cualidad);
			System.out.println("variable:" + variable);
			System.out.println("codigoEstacion:" + codigoEstacion);
			System.out.println("codsitio:" + sitio);*/
		
			try {
				conn = cFactory.createConnection();
				// System.out.println("SQL de datos a graficar:"+SQL_);
				pstmt = conn.prepareStatement(SQL_);

				if (nivel == 1) {
					pstmt.setString(1, sitio);
					pstmt.setString(2, unidadManejo);
					pstmt.setString(3, sector);
					pstmt.setString(4, codigoEstacion);
					pstmt.setString(5, variable);
					pstmt.setString(6, cualidad);
				} else {
					pstmt.setString(1, sitio);
					pstmt.setString(2, variable);
					pstmt.setString(3, cualidad);
				}

				rs = pstmt.executeQuery();

				arrayData = new JSONArray();

				listaDatos = new ArrayList();

				String unit="";
				while (rs.next()) {
					long year = new Year(rs.getInt("ANO")).getFirstMillisecond();
					
					yintervalseries.add(year, rs.getDouble("PROM"),rs.getDouble("PROM") - rs.getDouble("desvest"),rs.getDouble("PROM") + rs.getDouble("desvest"));

					objsData = new JSONObject();
					objsData.put("anio", rs.getString("ANO"));
					objsData.put("desvest", rs.getString("desvest"));
					objsData.put("prom", rs.getString("PROM"));
					objsData.put("max", rs.getString("max"));
					objsData.put("mix", rs.getString("mix"));
					objsData.put("unidad", rs.getString("unidad"));
					arrayData.put(objsData);
					
					
					listaDatos.add(rs.getString("ANO") + ","+ rs.getString("desvest") + ","+ rs.getString("PROM") + "," + rs.getString("max")+ "," + rs.getString("mix"));
					unit=rs.getString("unidad");
				}
				measureUnit=unit;
				data = arrayData.toString();
				

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				ConnectionFactory.closeConnection(conn);
			}
		}
		return yintervalseries;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public ArrayList getListaDatos() {
		return listaDatos;
	}

	public void setListaDatos(ArrayList listaDatos) {
		this.listaDatos = listaDatos;
	}

	public String getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}

}

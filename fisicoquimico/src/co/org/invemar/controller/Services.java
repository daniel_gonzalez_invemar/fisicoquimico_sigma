package co.org.invemar.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionFactory;
import co.org.invemar.util.actions.ActionRouter;

public class Services extends HttpServlet {
	private final static String CLASSPACKAGE="co.org.invemar.controller.action.";
	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.flush();
		out.close();*/
		
		response.setContentType("text/html");
		String action=request.getParameter("action");
		//System.out.println("action: "+action);
		String className=CLASSPACKAGE+action+"Action";
		
		try {
			Action actions = ActionFactory.getAction(className);
			
			ActionRouter router = actions.execute(request, response);
			if(router!=null)
				router.route(this, request, response);
			
			
		} catch(Exception e) {
			throw new ServletException(e);
		}
		
		
		
		/**/
		
		
		
	}

}

package co.org.invemar.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.library.siam.metadatos.Metadatos;
import co.org.invemar.library.siam.vo.Responsable;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class InvestigadoresAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		
		try {
			 String periodos = request.getParameter("anio");
			 String codSector = request.getParameter("codsector");
			
			 
			 long codSectorl = Long.parseLong(codSector);
			 
			 Metadatos metadatos = new Metadatos();
			
			 
			 ConnectionFactory cFactory = new ConnectionFactory();
			 Connection conn = cFactory.createConnection();
			 
			 ArrayList<Responsable> responsables = metadatos.getResponsableDatosBySector(conn,codSectorl,periodos,"86");
			 
			 Iterator<Responsable> it = responsables.iterator();	
			 JSONArray arrayJSON  = new JSONArray();
			
			
			  while(it.hasNext()){
				   JSONObject object = new JSONObject();
				   Responsable responsable = it.next();
				   object.accumulate("nombre", responsable.getNombre()+" "+responsable.getApellido()+"-"+responsable.getVinculado() );	
				   arrayJSON.put(object);
				   
			  }
			  
			 
			  
			 response.setContentType("text/text; charset=ISO-8859-1");
			 response.setHeader("Cache-Control", "no-cache");
			 response.getWriter().write(arrayJSON.toString());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		// return new ActionRouter("/grafica.jsp",false);
		return null;
	}

}

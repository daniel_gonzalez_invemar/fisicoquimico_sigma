package co.org.invemar.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import co.org.invemar.model.DataChartManager;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class SitioAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String resp=null;
		
		
		DataChartManager dm=new DataChartManager();
		String unidadManejo= request.getParameter("um");
		int idUnidadManejo= Integer.parseInt(unidadManejo);
		
		String parametrobusqueda = request.getParameter("parametrobusqueda");
		int parametrobusquedaInt = 0;
		if (parametrobusqueda != null) {
			parametrobusquedaInt = Integer.parseInt(parametrobusqueda);
		}
		
		try {
			resp=dm.findSitios(parametrobusquedaInt,idUnidadManejo);
		} catch (JSONException e) {
		
			e.printStackTrace();
		}
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(resp);
		return null;
	}

}

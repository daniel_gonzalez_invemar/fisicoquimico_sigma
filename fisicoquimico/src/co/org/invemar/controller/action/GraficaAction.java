package co.org.invemar.controller.action;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;

import co.org.invemar.library.siam.exportadores.ExportadorExcel;
import co.org.invemar.model.DataChartManager;
import co.org.invemar.model.DeviationChart;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class GraficaAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		JFreeChart jfreechart = null;
		try {
			// sitio=LUN&unidad=ZP1&sector=CGSM&estacion=39489&variable=CON&cualidad=Sup
			String sitio = request.getParameter("sitio");
			String unidadManejo = request.getParameter("unidad");
			String sector = request.getParameter("sector");
			String codigoEstacion = request.getParameter("estacion");
			String variable = request.getParameter("variable");
			String cualidad = request.getParameter("cualidad");
			String formato = request.getParameter("formato");
			String parametrobusqueda = request.getParameter("tipobusqueda");
			int parametrobusquedaInt = 0;
			if (parametrobusqueda != null) {
				parametrobusquedaInt = Integer.parseInt(parametrobusqueda);
			}
			
            			
			DeviationChart deviation = new DeviationChart();
			jfreechart = deviation.createChartFisicoquimicoByAnyo(
					parametrobusquedaInt,
					"Promedios de los valores registrados", "Fecha", "Unidad",
					variable, sitio, unidadManejo, sector, codigoEstacion,
					variable, cualidad);
			
			String data = deviation.getData();			
			ArrayList listaDatos = deviation.getListaDatos();
			
			

			if (formato.equals("img")) {
				OutputStream out = response.getOutputStream();
				response.setContentType("image/png");
				ChartUtilities.writeChartAsPNG(out, jfreechart, 850, 600);
				
			} else if (formato.equals("data")) {
				response.setContentType("text/text; charset=ISO-8859-1");
				response.setHeader("Cache-Control", "no-cache");
				if (data!=null){
					response.getWriter().write(data);
				}
				
				
			} else if (formato.equals("exporta")) {

				if (listaDatos.size() != 0) {
					ExportadorExcel exportador = new ExportadorExcel("Datos",
							listaDatos,
							"Anio,Desviacion,Promedio,M�ximo,M�nimo");
					exportador.setMetadataFirtSheet("Parametros: Sector: "
							+ sector + " Sitio:" + sitio + " Unidad de manejo:"
							+ unidadManejo + " Estacion:" + codigoEstacion
							+ " Variable:" + variable + " Cualiad:" + cualidad);

					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition:",
							"attachment;filename=Datos.xls");
					response.setHeader("Cache-Control", "no-cache");
					exportador.generaArchivo();

					HSSFWorkbook workbook = exportador.getWorkbook();

					workbook.write(response.getOutputStream());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		// return new ActionRouter("/grafica.jsp",false);
		return null;
	}

}

package co.org.invemar.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import co.org.invemar.model.DataChartManager;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class CargaSectorAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String resp=null;
		String parametroBusqueda = request.getParameter("parametrobusqueda");
		String codDepartamento = request.getParameter("codDepartamento");
		
		int parametrobusqueda = Integer.parseInt(parametroBusqueda);		
		DataChartManager dm=new DataChartManager();
				
			try {
				resp=dm.findSector(parametrobusqueda,codDepartamento);
			} catch (JSONException e) {				
				e.printStackTrace();
			}
		
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(resp);
		
		return null;
	}

}

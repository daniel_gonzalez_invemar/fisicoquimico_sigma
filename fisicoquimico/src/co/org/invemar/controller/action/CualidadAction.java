package co.org.invemar.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import co.org.invemar.model.DataChartManager;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class CualidadAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String resp=null;
		String sitio=request.getParameter("sitio");
		String unidad=request.getParameter("unidad");
		String sector=request.getParameter("sector");
		String estacion=request.getParameter("estacion");
		String variable=request.getParameter("variable");
		String parametrobusqueda=request.getParameter("parametrobusqueda");
		int   parametrobusquedaInt =Integer.parseInt(parametrobusqueda);
		
		
		
		
		DataChartManager dm=new DataChartManager();
		try {
			resp=dm.findCualidades(parametrobusquedaInt,sitio, unidad, sector, estacion, variable);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(resp);
		return null;
	}

}

package co.org.invemar.controller.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import co.org.invemar.model.DataChartManager;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class LoadSiteBySectorAction implements Action {

	public ActionRouter execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		String resp=null;
		
		
		DataChartManager dm=new DataChartManager();
		String sector= request.getParameter("sector");
		int codsector = Integer.parseInt(sector);
		
		String parametroBusqueda = request.getParameter("parametrobusqueda");
		int parametrobusquedaInt =0;
		
		if (parametroBusqueda!=null){
			parametrobusquedaInt=Integer.parseInt(parametroBusqueda);
		}
		
		try {
			resp=dm.findUnidadManejo(parametrobusquedaInt,codsector);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.setContentType("text/text; charset=ISO-8859-1");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(resp);
		return null;
	}

}
